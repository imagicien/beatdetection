import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;  
AudioPlayer song;
FFT fft;

int nbBandes;

void setup()
{
  size(800, 600, P3D);
  
  minim = new Minim(this);
  song = minim.loadFile("../data/marcus_kellis_theme.mp3", 2048);
    // marcus_kellis_theme.mp3  gone.mp3  christmascore.aiff  hey.mp3
  song.loop();
  
  // create an FFT object
  fft = new FFT( song.bufferSize(), song.sampleRate() );
  
  nbBandes = binlog(fft.specSize());
  
  ellipseMode(RADIUS);
  eRadius = 20;
  
  textAlign(RIGHT);
}

float CalculerEnergie(int indexBande)
{
  float sommeDesCarres = 0;
  int debut = (int)Math.pow(2, indexBande)-1;
  int fin = (int)Math.pow(2, indexBande+1)-1;
  for(int i = debut; i < fin; i++)
  {
    float ampl = fft.getBand(i);
    sommeDesCarres += ampl*ampl;
  }
  return sommeDesCarres * (fin-debut) / fft.specSize();
}

float eRadius;
Boolean wheelMode = true;

RingBuffer historiqueBasse = new RingBuffer(43);
float facteurSeuil = 1.0;
int maxBand = 3;
Boolean isBeating = false;    // Indique qu'un beat est en cours
int noBeatNb = 0;             // Nombre de buffers sans beat après le beat
int nbBeats = 0;

color green = color(60, 255, 0);
color red = color(255, 0, 0);
color yellow = color(255, 255, 0);

void draw()
{
  background(0);
  
  fft.forward( song.mix );
  
  // Traitement selon l'énergie des basses -------------------------
  Boolean beat = false;
  // 1. Calculer l'énergie des basses
  float energie = 0;
  for(int i = 0; i < maxBand; i++)
  {
    energie += CalculerEnergie(i);
  }
  // 2. Calculer statistiques sur l'historique
  float moyenne = historiqueBasse.getMoyenne();
  float variance = historiqueBasse.getVariance();
  float ecartType = sqrt(variance);
  // 3. Maj de l'historique
  historiqueBasse.push(energie);
  // 4. Calculer seuil
  float seuil = moyenne + facteurSeuil * ecartType;
  // 5. Déterminer la présence d'un pic
  if (energie > seuil)
  {
    isBeating = true;
    noBeatNb = 0;
  }
  else
  {
    if (isBeating || noBeatNb > 0)
    {
      noBeatNb++;
      if (noBeatNb == 2)
      {
        beat = true;
        noBeatNb = 0;
        nbBeats++;
      }
    }
    isBeating = false;
  }
  // ---------------------------------------------------------------
  
  fill(wheelMode ? yellow : green);
  text("FS: " + nf(facteurSeuil, 2, 2), 70, height - 40);
  fill(!wheelMode ? yellow : green);
  text("maxBand: " + maxBand, 170, height - 40);
  
  fill(green);
  text("Nb beats: " + nbBeats, 270, height - 40);
  
  // Faire reagir l'ellipse au beat
  fill( 255, 255, 0, map(eRadius, 20, 140, 80, 255) );
  if (beat) eRadius = 140;
  ellipse(width/2, height/2, eRadius, eRadius);
  eRadius *= 0.95;
  if ( eRadius < 20 ) eRadius = 20;
  
  text(round(frameRate), width - 50, 20);
}

void mouseClicked()
{
  if (mouseButton == LEFT)
    wheelMode = !wheelMode; 
}

void mouseWheel(MouseEvent event)
{
  float e = -event.getCount();
  
  if (wheelMode)   //  wheelMode A: facteurSeuil
    facteurSeuil += e * 0.1;
  else              // wheelMode B: maxBin
    maxBand += e;
}

class RingBuffer
{
  private int size; //86 = 2 sec, 43 = 1 sec
  private float[] buffer;
  private int index = 0; //position de la prochaine insertion
  private float somme = 0;
  private int elementsMoy = 0;
  private float maximum = 0;
  
  public RingBuffer()
  {
    size = 43;
    buffer = new float[size];
  }
  public RingBuffer(int iSize)
  {
    size = iSize;
    buffer = new float[size];
  }
  
  public void push(float val)
  {
    somme -= buffer[index];
    buffer[index] = val;
    somme += val;
    elementsMoy = (elementsMoy < size) ? ++elementsMoy : size;
    
    maximum = 0;
    for(int i = 0; i < size; i++)
    {
      if(buffer[i] > maximum) maximum = buffer[i];
    }
    
    index = (index < size-1) ? ++index : 0;
  }
  public float getMoyenne()
  {
    return somme / elementsMoy;
  }
  public float getMax()
  {
    return maximum;
  }
  public float getVariance()
  {
    float variance = 0;
    float moyenne = getMoyenne();
    for(int i = 0; i < size; i++)
    {
      variance += pow(buffer[i] - moyenne, 2);
    }
    return variance / size;
  }
  
  public void test()
  {
    println();
    println("RingBuffer Test");
    for(int i = 0; i < size*3; i++)
    {
      push(random(100));
      
      for(int j = 0; j < size; j++)
      {
        print(nf(buffer[j], 2, 1) + " ");
      }
      print("  moy: " + nf(getMoyenne(), 2, 1));
      println("  max: " + nf(getMax(), 2, 1));
    }
    println();
  }
};

public int binlog( int bits ) // returns 0 for bits=0
{
    int log = 0;
    if( ( bits & 0xffff0000 ) != 0 ) { bits >>>= 16; log = 16; }
    if( bits >= 256 ) { bits >>>= 8; log += 8; }
    if( bits >= 16  ) { bits >>>= 4; log += 4; }
    if( bits >= 4   ) { bits >>>= 2; log += 2; }
    return log + ( bits >>> 1 );
}